import React from 'react';
import ReactDOM from 'react-dom/client';
import { Provider } from 'react-redux';
import App from './App'
import storecinima from './Redux_Saga/Store/store';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <Provider store={storecinima}>
        <App />
    </Provider>
);
