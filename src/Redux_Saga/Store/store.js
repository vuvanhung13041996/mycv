import { applyMiddleware, combineReducers, createStore } from "redux";
import { reducermovienow } from "../Reducers/reducer";
import reduxSaga from "redux-saga";
import MiddleSaga from "../Saga/saga";

let cinimasaga = reduxSaga()
const allcinimastae = combineReducers({
    reducermovienow,
})

const storecinima = createStore(
    allcinimastae,
    applyMiddleware(cinimasaga)
)

cinimasaga.run(MiddleSaga)
export default storecinima
