import { call, put, takeLatest } from "@redux-saga/core/effects";

const HandleMovieNow = async (payload) => {
    console.log(payload);
    let link = 'https://teachingserver.onrender.com/cinema/'
    let api = link + payload

    let value = await fetch(`${api}`)
    let valueparse = await value.json()
    console.log(valueparse);
    return valueparse
}

function* GetDaTaMovieNow({ type, payload }) {
    let data = yield call(() => HandleMovieNow(payload))
    yield put({
        type: "GET",
        payload: data
    })
}

function* MiddleSaga() {
    yield takeLatest('GET_MOVIE_NOW', GetDaTaMovieNow);
    yield takeLatest('GET_CINEMAS', GetDaTaMovieNow)
    yield takeLatest('SON', GetDaTaMovieNow)
}

export default MiddleSaga