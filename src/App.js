import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import { BrowserRouter, Link, Route, Routes } from 'react-router-dom'
import Cinemas from './Components/Cinemas'
import Commingsoon from './Components/Commingsoon'
import Now from './Components/Now'


function App(props) {
  const [state, setState] = useState("GET_MOVIE_NOW")
  const [index, setIndex] = useState(0)
  let arr = ["cinemas/0000001005", "cinemas", "nowAndSoon"]
  console.log(props.movienow);

  // Handle Call Dispatch
  useEffect(() => props.HandleMovieNow(state, arr[index]), [state])
  const HandleClick = (content, i) => {
    setState(content)
    setIndex(i)
  }
  return (
    <BrowserRouter>
      <Link to={"/"} >Hello</Link>
      

      <button onClick={() => HandleClick('GET_MOVIE_NOW', 0)}><Link to={"/Now"}>Noww</Link></button>
      |
      <button onClick={() => HandleClick("GET_CINEMAS", 1)}><Link to={"/Cinemas"}>Cinemas</Link></button>
      |
      <button onClick={() => HandleClick("SON", 2)}><Link to={"/Commingsoon"}>Commingsoon</Link></button>



      <Routes>
        <Route path='/Now' element={<Now props={props.movienow} />} />
        <Route path='/Cinemas' element={<Cinemas props={props.movienow} />} />
        <Route path='/Commingsoon' element={<Commingsoon props={props.movienow} />} />
      </Routes>
    </BrowserRouter>
  )
}

// handle Props store
const MapStateToProps = (PropStore) => {
  return {
    movienow: PropStore.reducermovienow.listmovienow,
  }
}
// Handle Call API
const MapDispathToProps = (dispath) => {
  return (
    {
      HandleMovieNow: (type, payload) => {
        dispath({
          type: type,
          payload: payload
        })
      }
    }
  )
}


export default connect(MapStateToProps, MapDispathToProps)(App)