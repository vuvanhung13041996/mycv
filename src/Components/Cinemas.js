import React from 'react'
import './CSS/Cinemas.css'

export default function Cinemas(props) {
  return (

    <>
      {console.log(props.props)}
      <div>
        {(props.props.length > 1) ? props.props.map((element, index) => {
          return <div key={index}>
            <div>
              <hr />
              <h1>{element.vistaName}</h1>
              <h2>{element.address}</h2>
              <h3>{element.updatedBy}</h3>
              {element.imageUrls?.map(img =>
                <img src={img} key={img} />
              )}

            </div>
          </div>
        }) : undefined}
      </div>
    </>
  )
}
