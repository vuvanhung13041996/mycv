import React from 'react'
import './CSS/Cinemas.css'

export default function Commingsoon(props) {
    return (
        <div className='container-cmsoon'>{props.props.movieCommingSoon?.map(element => {
            let a = "Không Giới Hạn Độ Tuổi"
            return <div className="movie-cmsoon" key={element.id}>
                <img className="movie-cmsoon-img" src={element.imagePortrait} />
                <div className='movie-cmsoon-blockcontent'>
                    <h2 className="movie-cmsoon-name-movie">Phim: {element.name}</h2>
                    <h3 className="movie-cmsoon-age">Tuổi: {(element.age == 0) ? "Không Giới Hạn" : element.age}</h3>
                    <a className="movie-cmsoon-start" href={element.trailer}>Click Để xem Trailer</a>
                </div>
            </div>
        })}
        </div>
    )
}
