import './CSS/Now.css'
function Now(props) {

    return (
        <>
            <div className="movie-now-container">
                {props.props.map((element, index) => {
                    return <div className="movie-now" key={element.id}>
                        <img className="movie-now-img" src={element.imagePortrait} />
                        <div>
                            <h2 className="movie-now-name-movie">Phim: {element.subName || element.movieName}</h2>
                            <h3 className="movie-now-age">Tuổi: {element.age}</h3>
                            <h3 className="movie-now-start">Khỏi Chiếu: {element.startdate?.slice(0, 10)}</h3>
                        </div>
                    </div>
                })}
            </div>
        </>
    )
}



export default Now